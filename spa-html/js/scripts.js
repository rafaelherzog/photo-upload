const baseUrl = 'https://localhost:5001'
const baseApiUrl = 'https://localhost:5001/api';

var app = Vue.createApp({
    template: `
        <table class="tableUpload">
            <tr>
                <td>
                    <label>Selectd Item:</label>
                    <select v-model="selectedItem" v-show="!loading">
                        <option v-for="(item, k) in itemList" :value="item" v-bind:key="k">{{item.name}}</option>
                    </select>
                    <img v-show="loading" src="img/load.gif" />
                </td>
                <td v-for="(metal, k) in metalList" v-bind:key="k">{{metal}}</td>
            </tr>
            <tr v-for="(shape, k) in shapeList" v-bind:key="k">
                <td>{{shape}}</td>
                <td class="item" v-for="(metal, j) in metalList" v-bind:key="j">
                    <myItem v-bind:item="selectedItem" v-bind:metal="metal" v-bind:shape="shape" />
                </td>
            </tr>
        </table>
    `,
    data() {
        return {
            loading: true,
            propPhoto: {},
            selectedItem: { 'id':  0, 'name': '(none)', 'propPhoto': {} },
            itemList: [],
            metalList: ['Yellow Gold', 'Rose Gold'],
            shapeList: ['Round', 'Cushion'],
        };
    },
    created() {
        this.propPhoto[this.metalList[0]] = {};
        this.propPhoto[this.metalList[1]] = {};
        this.propPhoto[this.metalList[0]][this.shapeList[0]] = '(photo url)';
        this.propPhoto[this.metalList[1]][this.shapeList[0]] = '(photo url)';
        this.propPhoto[this.metalList[0]][this.shapeList[1]] = '(photo url)';
        this.propPhoto[this.metalList[1]][this.shapeList[1]] = '(photo url)';
        this.selectedItem.propPhoto = this.propPhoto;
    },
    methods: {
        fetchData() {
            this.loading = true;
            if(this.itemList.length == 0) {
                fetch(baseApiUrl + '/Item/', { method: 'get', mode: 'cors' })
                    .then(res => res.json())
                    .then(data => { this.itemList = data; this.itemList.map((item, key) => { item.propPhoto = this.propPhoto }); this.loading = false})
                    .catch(ex => console.log(ex.message));
                ;
            }
        },
    },
    mounted() {
        this.fetchData();
    },
});

app.component('myItem', {
    template: `
        <form @submit.prevent='handleUpload'>
            <!--<input required type='hidden' v-model='item.id' name='id_tem' />
            <input required type='hidden' v-model='metal' name='metal' />
            <input required type='hidden' v-model='shape' name='shape' />-->
            <input required type='file' id='uploadPhoto' name='uploadPhoto' accept='image/*' @change="onChange" >           
            <button v-show="!loading">Add Image</button>
            <img v-show="loading" src="img/load.gif" />
        </form>
        <img v-bind:src="thumbUrl" alt="(none)" />
    `,
    props: ['item', 'metal', 'shape'],
    data() {
        return {
            loading: false,
            imageFile: null,
            thumbUrl: ''
        };
    },
    methods: {
        onChange(e){
            this.imageFile = e.target.files[0];
        },

        handleUpload() {
            if(this.item.id > 0)
            {
                this.loading = true;

                const formData = new FormData();

                formData.append('id_tem', this.item.id);
                formData.append('metal', this.metal);
                formData.append('shape', this.shape);
                formData.append('imageFile', this.imageFile, this.imageFile.name);

                var requestOptions = { method: 'POST', mode: 'cors', body: formData };

                fetch(baseApiUrl + '/Img/', requestOptions)
                    .then(res => res.json())
                    .then(data => { this.thumbUrl = baseApiUrl + '/Img?id=' + data; this.loading = false })
                    .catch(ex => console.log(ex.message));
            }
            else {
                alert('Pelase, select item first');
            }

        },
    }
});

app.mount('#app');
