﻿using Diamonds.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Diamonds.WebAPI.Controllers
{
	public class ItemController : WebApiController
	{
		/*private class ItemReturn
		{
			public int id { get; set; }

			public string name { get; set; }

			public Dictionary<string, Dictionary<string, string>> propPhoto { get; set; }
		}*/

		[HttpGet]
		public IActionResult Get()
		{
			try
			{
				using (DiamondsDbContext db = DiamondsDbContext.GetDbContext())
				{
					var listItems = db.RepoItem.ListAll()
						.OrderBy(x => x.Name)
						.Select(x => new
						{
							id = x.ID,
							name = x.Name,
							propPhoto = new { }
						})
						.ToArray();

					/*foreach (var item in listItems)
					{
						item.propPhoto = db.RepoProperty.PropPhotoGet();

						foreach (string metal in item.propPhoto.Keys)
						{
							foreach (string shape in item.propPhoto[metal].Keys)
							{
								item.propPhoto[metal][shape] = db.RepoItemPhotoProperty.FindPhoto(item.id, metal, shape);
							}
						}
					}*/

					return Ok(listItems);
				}
			}
			catch (Exception ex)
			{
				return base.HandleEx(ex);
			}
		}
	}
}
