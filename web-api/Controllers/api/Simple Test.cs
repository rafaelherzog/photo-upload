﻿using Diamonds.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Diamonds.WebAPI.Controllers
{
	public class TestController : WebApiController
	{
		[HttpGet]
		public IActionResult TestGet()
		{
			try
			{
				using (DiamondsDbContext db = DiamondsDbContext.GetDbContext())
				{
					return Ok(Convert.ToString(
						db.RepoItemPhotoProperty.All.ToArray().Length +
						db.RepoItemPhoto.All.ToArray().Length +
						db.RepoItem.All.ToArray().Length +
						db.RepoProperty.All.ToArray().Length +
						db.RepoType.All.ToArray().Length +
						db.RepoTypeProperty.All.ToArray().Length));
				}
			}
			catch (Exception ex)
			{
				return base.HandleEx(ex);
			}
		}
	}
}
