﻿using Diamonds.Data;
using Diamonds.WebAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using ioFile = System.IO.File;

namespace Diamonds.WebAPI.Controllers
{
	public class ImgController : WebApiController
	{
		[HttpGet]
		public IActionResult ImgGet(int id)
		{
			try
			{
				using (DiamondsDbContext db = DiamondsDbContext.GetDbContext())
				{
					return File(ioFile.OpenRead(
						Path.Combine(Program.ContentRootPath, "upPhotos", id + "_" + db.RepoItemPhoto.FindFileName(id))), "image/png");
				}
			}
			catch (Exception ex)
			{
				return base.HandleEx(ex);
			}
		}

		[HttpPost]
		public IActionResult ImgPost(IFormCollection data, IFormFile imageFile)
		{
			try
			{
				PhotoItemService photoItemService = new PhotoItemService(Convert.ToInt32(data["id_tem"]), data["metal"], data["shape"], imageFile);

				return Ok(photoItemService.ItemPhotoEntityThumb.ID);
			}
			catch (Exception ex)
			{
				return base.HandleEx(ex);
			}
		}
	}
}
