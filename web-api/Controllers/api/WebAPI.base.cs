﻿using Diamonds.Helper;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Diamonds.WebAPI.Controllers
{
	[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
	[ApiController]
	[RequireHttps]
	[CORS_Attribute]
	[Route("api/[controller]")]
	public abstract class WebApiController : ControllerBase
	{
		protected IActionResult HandleEx(Exception ex)
		{
			return Problem(ex.FirstException().Message);
		}
	}
}
