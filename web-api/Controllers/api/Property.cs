﻿using Diamonds.Data;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Diamonds.WebAPI.Controllers
{
	public class PropertyController : WebApiController
	{
		[HttpGet]
		public IActionResult Get(int id)
		{
			try
			{
				using (DiamondsDbContext db = DiamondsDbContext.GetDbContext())
				{
					return Ok(db.RepoProperty.ListAllDistinct(id));
				}
			}
			catch (Exception ex)
			{
				return base.HandleEx(ex);
			}
		}
	}
}
