﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace Diamonds.WebAPI.Controllers
{
	/// <summary>
	/// https://en.wikipedia.org/wiki/Cross-origin_resource_sharing
	/// </summary>
	public class CORS_Attribute : ActionFilterAttribute
	{
		public override void OnResultExecuting(ResultExecutingContext filterContext)
		{
			filterContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");

			base.OnResultExecuting(filterContext);
		}
	}
}
