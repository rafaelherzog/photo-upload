﻿using Microsoft.AspNetCore.Mvc;

namespace Diamonds.WebAPI.Controllers
{
	[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
	public class HomeController : Controller
	{
		public IActionResult Index()
		{
			return View();
		}
	}
}
