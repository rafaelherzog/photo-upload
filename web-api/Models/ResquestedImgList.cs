using System;

namespace Diamonds.WebAPI.Models
{
	public class ResquestedImgListModel
	{
		public int ID_Item { get; set; }

		public string metal { get; set; }

		public string shape { get; set; }
	}
}
