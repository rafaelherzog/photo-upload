﻿using Diamonds.Data;
using Diamonds.Helper;
using Microsoft.AspNetCore.Http;
using System;
using System.Drawing.Imaging;
using System.IO;

namespace Diamonds.WebAPI.Services
{
	public class PhotoItemService
	{
		public ItemPhotoEntity ItemPhotoEntityPhoto { get; set; }

		public ItemPhotoEntity ItemPhotoEntityThumb { get; set; }

		public PhotoItemService(int id_item, string metal, string shape, IFormFile imageFile)
		{
			this.SaveData(id_item, metal, shape, imageFile);

			this.SaveFiles(imageFile);

		}

		private void SaveData(int id_item, string metal, string shape, IFormFile imageFile)
		{
			using (DiamondsDbContext db = DiamondsDbContext.GetDbContext())
			{
				// Save photo and thumb info to database

				this.ItemPhotoEntityPhoto = new ItemPhotoEntity()
				{
					ID_Item = id_item,
					TypeEnum = TypeEnum.Photo,
					FileName = "Photo_" + imageFile.FileName,
					Position = 1,
					IsActive = true,
					CreatedAt = DateTime.Now
				};

				this.ItemPhotoEntityThumb = new ItemPhotoEntity()
				{
					ID_Item = id_item,
					TypeEnum = TypeEnum.Thumb,
					FileName = "Thumb_" + imageFile.FileName + ".png",
					Position = 2,
					IsActive = true,
					CreatedAt = DateTime.Now
				};

				db.RepoItemPhoto.Save(this.ItemPhotoEntityPhoto);

				db.RepoItemPhoto.Save(this.ItemPhotoEntityThumb);

				db.SaveChanges();

				ItemPhotoPropertyEntity itemPhotoPropertyEntityMetal01 = new ItemPhotoPropertyEntity()
				{
					ID_ItemPhoto = this.ItemPhotoEntityPhoto.ID,
					ID_Property = (int)PropertyEnum.Metal,
					Value = metal
				};

				ItemPhotoPropertyEntity itemPhotoPropertyEntityMetal02 = new ItemPhotoPropertyEntity()
				{
					ID_ItemPhoto = this.ItemPhotoEntityPhoto.ID,
					ID_Property = (int)PropertyEnum.Shape,
					Value = shape
				};

				ItemPhotoPropertyEntity itemPhotoPropertyEntityMetal03 = new ItemPhotoPropertyEntity()
				{
					ID_ItemPhoto = this.ItemPhotoEntityThumb.ID,
					ID_Property = (int)PropertyEnum.Metal,
					Value = metal
				};

				ItemPhotoPropertyEntity itemPhotoPropertyEntityMetal04 = new ItemPhotoPropertyEntity()
				{
					ID_ItemPhoto = this.ItemPhotoEntityThumb.ID,
					ID_Property = (int)PropertyEnum.Shape,
					Value = shape
				};

				db.RepoItemPhotoProperty.Save(itemPhotoPropertyEntityMetal01);

				db.RepoItemPhotoProperty.Save(itemPhotoPropertyEntityMetal02);

				db.RepoItemPhotoProperty.Save(itemPhotoPropertyEntityMetal03);

				db.RepoItemPhotoProperty.Save(itemPhotoPropertyEntityMetal04);

				db.SaveChanges();
			}
		}

		private void SaveFiles(IFormFile imageFile)
		{
			string[] fileNames = new string[]
				{
					this.ItemPhotoEntityPhoto.ID + "_" + this.ItemPhotoEntityPhoto.FileName,
					this.ItemPhotoEntityThumb.ID + "_" + this.ItemPhotoEntityThumb.FileName
				};

			// Save photo to disk

			using (Stream fileStream = new FileStream(Path.Combine(Program.ContentRootPath, "upPhotos", fileNames[0]), FileMode.Create))
			{
				imageFile.CopyTo(fileStream);
			}

			// Generate thumb and save to disk

			using (Stream thumbStream = ImageHelper.Crop(120, 120, imageFile.OpenReadStream(), ImageFormat.Png))
			{
				using (Stream fileStream = new FileStream(Path.Combine(Program.ContentRootPath, "upPhotos", fileNames[1]), FileMode.Create))
				{
					thumbStream.CopyTo(fileStream);
				}
			}
		}
	}
}
