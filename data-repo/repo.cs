﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Diamonds.Data
{
	/// <summary>
	/// Repository Pattern
	/// </summary>
	/// <typeparam name="TDbEntity">DiamondsEntity</typeparam>
	public abstract class DiamondsRepository<TDbEntity>
		where TDbEntity : DiamondEntity, new()
	{
		protected DiamondsDbContext Context;

		public DbSet<TDbEntity> All;

		protected DiamondsRepository(DiamondsDbContext dbContext)
		{
			this.All = dbContext.Set<TDbEntity>();

			this.Context = dbContext;
		}

		//-----------------------------------------------------------------------		

		public void Rollback(TDbEntity dbEntity)
		{
			this.Context.Entry(dbEntity).Reload();
		}

		//-----------------------------------------------------------------------

		public virtual TDbEntity GetDbEntity(int id)
		{
			return this.All.Find(id);
		}

		public TDbEntity GetDbEntity(int id, string include)
		{
			return this.All.Include(include).Single(x => x.ID == id);
		}

		//-----------------------------------------------------------------------

		public TDbEntity Save(TDbEntity dbEntity)
		{
			if (dbEntity.ID == 0)
			{
				this.All.Add(dbEntity);
			}
			else
			{
				this.Context.Entry(dbEntity).State = EntityState.Modified;
			}

			return dbEntity;
		}

		//-----------------------------------------------------------------------

		public void Delete(TDbEntity dbEntity)
		{
			EntityState entityState = this.Context.Entry(dbEntity).State;

			if ((entityState != EntityState.Deleted) || (entityState != EntityState.Detached))
			{
				this.All.Remove(dbEntity);
			}
		}

		public void Delete(TDbEntity dbEntity, out EntityState entityState)
		{
			entityState = this.Context.Entry(dbEntity).State;

			if ((entityState != EntityState.Deleted) || (entityState != EntityState.Detached))
			{
				this.All.Remove(dbEntity);
			}
		}

		//-----------------------------------------------------------------------
	}
}
