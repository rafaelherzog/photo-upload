﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Diamonds.Data
{
	[Table("ItemPhotos")]
	public class ItemPhotoEntity : DiamondEntity
	{
		[Column("ItemId")]
		public virtual int? ID_Item { get; set; }

		[ForeignKey("ID_Item")]
		public virtual ItemEntity Item { get; set; }

		[Column("TypeId")]
		public virtual int ID_Type { get; set; }

		[ForeignKey("ID_Type")]
		public virtual TypeEntity Type { get; set; }

		[NotMapped]
		public virtual TypeEnum TypeEnum
		{
			set
			{
				this.ID_Type = (int)value;
			}
		}

		public virtual string FileName { get; set; }

		public virtual int? Position { get; set; }

		public virtual string Alt { get; set; }

		public virtual DateTime CreatedAt { get; set; }

		public virtual DateTime? ModifiedAt { get; set; }

		public virtual bool IsActive { get; set; }

		[InverseProperty("ItemPhoto")]
		public virtual ObservableCollection<ItemPhotoPropertyEntity> ItemPhotoProperty { get; set; }

		public ItemPhotoEntity()
		{
			this.ItemPhotoProperty = new ObservableCollection<ItemPhotoPropertyEntity>();
		}

		public override string ToString()
		{
			return string.Format("{0} - {1}", this.ID, this.FileName);
		}
	}

	public partial class DiamondsDbContext
	{
		protected virtual DbSet<ItemPhotoEntity> DbSetItemPhoto { get; set; }

		private ItemPhotoRepo repoItemPhoto;

		public ItemPhotoRepo RepoItemPhoto
		{
			get { return repoItemPhoto ?? (repoItemPhoto = new ItemPhotoRepo(this)); }
		}
	}

	public partial class ItemPhotoRepo : DiamondsRepository<ItemPhotoEntity>
	{
		internal ItemPhotoRepo(DiamondsDbContext dbContext) : base(dbContext)
		{

		}

		public string FindFileName(int id_ItemPhoto)
		{
			return base.All.Where(x => x.ID == id_ItemPhoto).Select(x => x.FileName).SingleOrDefault();
		}
	}
}
