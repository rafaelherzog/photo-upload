﻿using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Diamonds.Data
{
	[Table("Items")]
	public class ItemEntity : DiamondEntity
	{
		public virtual string Name { get; set; }

		[InverseProperty("Item")]
		public virtual ObservableCollection<ItemPhotoEntity> ItemPhoto { get; set; }

		public ItemEntity()
		{
			this.ItemPhoto = new ObservableCollection<ItemPhotoEntity>();
		}

		public override string ToString()
		{
			return string.Format("{0} - {1}", this.ID, this.Name);
		}
	}

	public partial class DiamondsDbContext
	{
		protected virtual DbSet<ItemEntity> DbSetItem { get; set; }

		private ItemRepo repoItem;

		public ItemRepo RepoItem
		{
			get { return repoItem ?? (repoItem = new ItemRepo(this)); }
		}
	}

	public partial class ItemRepo : DiamondsRepository<ItemEntity>
	{
		internal ItemRepo(DiamondsDbContext dbContext) : base(dbContext)
		{

		}

		public IEnumerable<ItemEntity> ListAll()
		{
			return base.All;
		}
	}
}
