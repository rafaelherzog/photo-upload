﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Diamonds.Data
{
	[Table("ItemPhotoPropertySet")]
	public class ItemPhotoPropertyEntity : DiamondEntity
	{
		[Column("ItemPhotoId")]
		public virtual int ID_ItemPhoto { get; set; }

		[ForeignKey("ID_ItemPhoto")]
		public virtual ItemPhotoEntity ItemPhoto { get; set; }

		[Column("PropertyId")]
		public virtual int ID_Property { get; set; }

		[ForeignKey("ID_Property")]
		public virtual PropertyEntity Property { get; set; }

		public virtual string Value { get; set; }

		public ItemPhotoPropertyEntity()
		{

		}

		public override string ToString()
		{
			return string.Format("{0} - {1}", this.ID, this.Value);
		}
	}

	public partial class DiamondsDbContext
	{
		protected virtual DbSet<ItemPhotoPropertyEntity> DbSetItemPhotoProperty { get; set; }

		private ItemPhotoPropertyRepo repoItemPhotoProperty;

		public ItemPhotoPropertyRepo RepoItemPhotoProperty
		{
			get { return repoItemPhotoProperty ?? (repoItemPhotoProperty = new ItemPhotoPropertyRepo(this)); }
		}
	}

	public partial class ItemPhotoPropertyRepo : DiamondsRepository<ItemPhotoPropertyEntity>
	{
		internal ItemPhotoPropertyRepo(DiamondsDbContext dbContext) : base(dbContext)
		{

		}

		public string FindPhoto(int idItem, string metal, string shape)
		{
			var photosMetal = base.All.Where(x => x.ItemPhoto.ID_Item == idItem && x.ItemPhoto.ID_Type == 2 && x.ID_Property == 1 && x.Value == metal).Select(x => x.ID_ItemPhoto);

			var photosShape = base.All.Where(x => x.ItemPhoto.ID_Item == idItem && x.ItemPhoto.ID_Type == 2 && x.ID_Property == 2 && x.Value == shape).Select(x => x.ID_ItemPhoto);

			int? intersect = photosMetal.Intersect(photosShape).SingleOrDefault();

			if ((intersect ?? 0) > 0)
			{
				int idThumb = intersect.Value;

				return base.Context.RepoItemPhoto.All.Where(x => x.ID == idThumb).Select(x => x.FileName).Single();
			}
			else
			{
				return "(nome)";
			}
		}
	}
}
