﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Diamonds.Data
{
	[Table("Properties")]
	public class PropertyEntity : DiamondEntity
	{
		public virtual string Name { get; set; }

		[InverseProperty("Property")]
		public virtual ObservableCollection<TypePropertyEntity> TypeProperty { get; set; }

		[InverseProperty("Property")]
		public virtual ObservableCollection<ItemPhotoPropertyEntity> ItemPhotoProperty { get; set; }

		public PropertyEntity()
		{
			this.TypeProperty = new ObservableCollection<TypePropertyEntity>();

			this.ItemPhotoProperty = new ObservableCollection<ItemPhotoPropertyEntity>();
		}

		public override string ToString()
		{
			return string.Format("{0} - {1}", this.ID, this.Name);
		}
	}

	public partial class DiamondsDbContext
	{
		protected virtual DbSet<PropertyEntity> DbSetProperty { get; set; }

		private PropertyRepo repoProperty;

		public PropertyRepo RepoProperty
		{
			get { return repoProperty ?? (repoProperty = new PropertyRepo(this)); }
		}
	}

	public enum PropertyEnum
	{
		Metal = 1,
		Shape = 2
	}

	public partial class PropertyRepo : DiamondsRepository<PropertyEntity>
	{
		internal PropertyRepo(DiamondsDbContext dbContext) : base(dbContext)
		{

		}

		/// <summary>
		/// List all available properties
		/// </summary>
		/// <param name="propertyEnum"></param>
		/// <returns></returns>
		public string[] ListAllDistinct(PropertyEnum propertyEnum)
		{
			int id_Property = (int)propertyEnum;

			return base.Context.RepoItemPhotoProperty.All
				.Where(x => x.ID_Property == id_Property)
				.Select(x => x.Value)
				.Distinct()
				.OrderBy(x => x)
				.ToArray();
		}

		/// <summary>
		/// List all available properties
		/// </summary>
		/// <param name="id_Property"></param>
		/// <returns></returns>
		public string[] ListAllDistinct(int id_Property)
		{
			return base.Context.RepoItemPhotoProperty.All
				.Where(x => x.ID_Property == id_Property)
				.Select(x => x.Value)
				.Distinct()
				.OrderBy(x => x)
				.ToArray();
		}

		private static string[] metalEnum = new string[] { "Yellow Gold", "Rose Gold" };

		private static string[] shapeEnum = new string[] { "Round", "Cushion" };

		public Dictionary<string, Dictionary<string, string>> PropPhotoGet()
		{
			Dictionary<string, Dictionary<string, string>> propPhoto = new Dictionary<string, Dictionary<string, string>>();

			foreach (string metal in metalEnum)
			{
				propPhoto.Add(metal, new Dictionary<string, string>());

				foreach (string shape in shapeEnum)
				{
					propPhoto[metal].Add(shape, null);
				}
			}

			return propPhoto;
		}
	}
}
