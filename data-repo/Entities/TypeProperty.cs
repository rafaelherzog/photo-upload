﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace Diamonds.Data
{
	[Table("TypePropertySet")]
	public class TypePropertyEntity : DiamondEntity
	{
		[Column("MediaTypeId")]
		public virtual int ID_Type { get; set; }

		[ForeignKey("ID_Type")]
		public virtual TypeEntity Type { get; set; }

		[Column("PropertyId")]
		public virtual int ID_Property { get; set; }

		[ForeignKey("ID_Property")]
		public virtual PropertyEntity Property { get; set; }

		public TypePropertyEntity()
		{

		}

		public override string ToString()
		{
			return string.Format("{0} / {1}",
				this.ID_Type == 0 ? "-" : this.Type.ToString(),
				this.ID_Property == 0 ? "-" : this.Property.ToString());
		}
	}

	public partial class DiamondsDbContext
	{
		protected virtual DbSet<TypePropertyEntity> DbSetTypeProperty { get; set; }

		private TypePropertyRepo repoTypeProperty;

		public TypePropertyRepo RepoTypeProperty
		{
			get { return repoTypeProperty ?? (repoTypeProperty = new TypePropertyRepo(this)); }
		}
	}

	public partial class TypePropertyRepo : DiamondsRepository<TypePropertyEntity>
	{
		internal TypePropertyRepo(DiamondsDbContext dbContext) : base(dbContext)
		{

		}
	}
}
