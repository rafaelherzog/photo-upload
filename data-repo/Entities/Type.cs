﻿using Microsoft.EntityFrameworkCore;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Diamonds.Data
{
	[Table("Types")]
	public class TypeEntity : DiamondEntity
	{
		public virtual string Name { get; set; }

		[InverseProperty("Type")]
		public virtual ObservableCollection<ItemPhotoEntity> ItemPhoto { get; set; }

		[InverseProperty("Type")]
		public virtual ObservableCollection<TypePropertyEntity> TypeProperty { get; set; }

		public TypeEntity()
		{
			this.ItemPhoto = new ObservableCollection<ItemPhotoEntity>();

			this.TypeProperty = new ObservableCollection<TypePropertyEntity>();
		}

		public override string ToString()
		{
			return string.Format("{0} - {1}", this.ID, this.Name);
		}
	}

	public enum TypeEnum
	{
		Photo = 1,
		Thumb = 2
	}

	public partial class DiamondsDbContext
	{
		protected virtual DbSet<TypeEntity> DbSetType { get; set; }

		private TypeRepo repoType;

		public TypeRepo RepoType
		{
			get { return repoType ?? (repoType = new TypeRepo(this)); }
		}
	}

	public partial class TypeRepo : DiamondsRepository<TypeEntity>
	{
		internal TypeRepo(DiamondsDbContext dbContext) : base(dbContext)
		{

		}
	}
}
