﻿using Microsoft.EntityFrameworkCore;

namespace Diamonds.Data
{
	public partial class DiamondsDbContext : DbContext
	{
		private static string DiamondsDbContextConnectionString;

		public static void SetUp(string diamondsDbContextConnectionString)
		{
			DiamondsDbContextConnectionString = diamondsDbContextConnectionString;
		}

		public static DiamondsDbContext GetDbContext()
		{
			return new DiamondsDbContext();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder.UseSqlServer(DiamondsDbContextConnectionString));
		}

		//----------------------------------------------------------------------
		internal DiamondsDbContext()
		{
			this.OkToSaveChanges = true;
		}

		private bool OkToSaveChanges;

		/// <summary>
		/// Changes to the database will not be saved
		/// </summary>
		public void NotOkToSave()
		{
			this.OkToSaveChanges = false;
		}

		/// <summary>
		/// Saves changes to the database and disposes the object
		/// </summary>
		public override void Dispose()
		{
			if (this.OkToSaveChanges)
			{
				base.SaveChanges();
			}

			base.Dispose();
		}
	}
}
