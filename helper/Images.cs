﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Diamonds.Helper
{
	public static class ImageHelper
	{
		public static Stream Crop(int width, int height, Stream streamImg, ImageFormat format)
		{
			MemoryStream outStream = new MemoryStream();

			using (Bitmap sourceImage = new Bitmap(streamImg))
			{
				using (Bitmap objBitmap = new Bitmap(width, height))
				{
					objBitmap.SetResolution(sourceImage.HorizontalResolution, sourceImage.VerticalResolution);

					using (Graphics objGraphics = Graphics.FromImage(objBitmap))
					{
						objGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

						objGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

						objGraphics.DrawImage(sourceImage, 0, 0, width, height);

						objBitmap.Save(outStream, format);
					}
				}
			}

			outStream.Position = 0;

			return outStream;
		}
	}
}
