﻿using System;

namespace Diamonds.Helper
{
	public static class ExceptionsEx
	{
		public static Exception FirstException(this Exception exception)
		{
			int k = 0;

			Exception aux = exception;

			while ((aux.InnerException != null) && (k < 100))
			{
				aux = aux.InnerException;

				k++;
			}

			return aux;
		}
	}
}
